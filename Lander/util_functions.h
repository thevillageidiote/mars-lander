#pragma once


void indicateEulerReset(vector3d *second_last_position);
void generateInitialConditions(vector3d *second_last_position);





double getLanderMass(void);
vector3d Gravitational_Force(vector3d *position);
vector3d Drag_Force_Lander(vector3d *velocity, vector3d *position);
vector3d Drag_Force_Parachute(vector3d *velocity, vector3d *position);
