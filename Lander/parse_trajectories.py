import numpy as np
import matplotlib.pyplot as plt
results = np.loadtxt('output.txt')
plt.figure(1)
plt.clf()
plt.xlabel('Velocity (m s-1)')
plt.ylabel('Height (m)')
plt.grid()
plt.plot(results[:, 1], results[:, 0], label='x (m)')
#plt.plot(results[:, 0], results[:, 2], label='v (m/s)')
plt.legend()
plt.show()
