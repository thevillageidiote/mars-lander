// Mars lander simulator
// Version 1.9
// Mechanical simulation functions
// Gabor Csanyi and Andrew Gee, August 2016

// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation, to make use of it
// for non-commercial purposes, provided that (a) its original authorship
// is acknowledged and (b) no modified versions of the source code are
// published. Restriction (b) is designed to protect the integrity of the
// exercise for future generations of students. The authors would be happy
// to receive any suggested modifications by private correspondence to
// ahg@eng.cam.ac.uk and gc121@eng.cam.ac.uk.

#include "lander.h"
#include "util_functions.h"
#include "math.h"


//Static declarations of second_last_position for euler integration
static vector3d second_last_position;

static double vertical_integral = 0;
static double horizontal_integral = 0;

//Static declarations of autopilot variables. Used to control Orbital Reentry via manual firing of the engines


void autopilot(void)
// Autopilot to adjust the engine throttle, parachute and attitude control
{
	// INSERT YOUR CODE HERE	

	  //Useful vector math

	vector3d s, tv, desired_h_vel, desired_v_vel;

	s = position.norm();

	//PID Loop values
	delta = Gravitational_Force(&position).abs() / MAX_THRUST;

	double h = position.abs() - MARS_RADIUS;
	double vertical_velocity = velocity * (position.norm());

	//Safety Optimization Code
	if (safety_enabled && !safety_optimised)
	{
		if (h > 3000 && vertical_velocity < 0)
		{
			//Various parameters to reset vertical_Kh

			//double critical_velocity;

			double critical_velocity = 0.015 * h + vertical_velocity / 100;

			if (h > 10000) critical_velocity = 0.002*h + vertical_velocity / 1000 + 150;

			if (h > 5000000) critical_velocity = 0.0006 * h + 150 + vertical_velocity;

			vertical_Kh = (critical_velocity) / (h);

			if (tv.abs() > 100 && h < 7000)
			{

				//horizontal_Kh = h / 3500000;

			}


			safety_optimised = true;
		}
		else safety_optimised = false;
	}

	//Set desired v vel
	double desired_v_velocity = (0.5 + vertical_Kh * h);

	//Set desired H vel
	double desired_h_velocity;
	// Direction of tangential velocity.Could be small if craft is falling vertically.
	tv = velocity - (velocity*s)*s;

	//Orbital Reentry Code
	if (orbital_reentry && orbital_reentry_count < 2000) {
		stabilized_attitude = 0;
		orientation = setOrientation(-tv.norm());

		throttle = 1;
		orbital_reentry_count++;
	}
	else if (orbital_reentry && orbital_reentry_count >= 2000)
	{
		orbital_reentry = false;
		orbital_reentry_count = 0;
		throttle = 0;
		stabilized_attitude = 0;
	}

	else if (!orbital_reentry)
	{ 
		
		//Code for normal descent with horizonal stabilization
		if (tv.abs() >= 0.5) {
			desired_h_velocity = (0.5 + horizontal_Kh * h);
		}
		else desired_h_velocity = 0;

		double vertical_error = -(desired_v_velocity + vertical_velocity);
		double vertical_Pout = Kp * vertical_error;
		
		double horizontal_error = (tv.abs() - desired_h_velocity);
		double horizontal_Pout = Kp*horizontal_error;


		if (vertical_Pout <= -delta)
		{
			stabilized_attitude = 0;
			orientation = setOrientation(-tv.norm());

			if (horizontal_Pout <= 0) throttle = 0;
			if (horizontal_Pout >= 1) throttle = 1;
			else throttle = horizontal_Pout;
		}
		else if (vertical_Pout >= (1 - delta))
		{
			//stabilize craft pointing up
			stabilized_attitude = 1;
			stabilized_attitude_angle = 0;
			throttle = 1;
			if (safe_to_deploy_parachute() && h < 7000)	parachute_status = DEPLOYED;
		}
		else {

			//We only need to turn on the integral controller when the craft is low
			if (h < 2000000)
			{
				vertical_integral = vertical_integral + vertical_error;
				vertical_Pout = Kp * vertical_error + vertical_Ki * vertical_integral;

			}
			

			//find angle
			double angle;
			if (horizontal_Pout > 0.1 || horizontal_Pout < -0.1) {

				//horizontal_integral = horizontal_integral + horizontal_error;
				//horizontal_Pout = Kp*horizontal_error + horizontal_Ki * horizontal_integral;
				if (position.z > 0) {
					angle = -atan(horizontal_Pout / vertical_Pout);
				}
				else if (position.z < 0) {
					angle = -atan(-horizontal_Pout / vertical_Pout);
				}
			}
			else angle = 0;

			//stabilize craft
			stabilized_attitude = 1;
			stabilized_attitude_angle = angle;

			//set throttle
			throttle = sqrt(pow(horizontal_Pout, 2) + pow(vertical_Pout + delta, 2));
			if (throttle >= 1) throttle = 1;

			if (safe_to_deploy_parachute() && h < 10000) parachute_status = DEPLOYED;
		}
	
	}

	
	
	

}

void numerical_dynamics (void)
  // This is the function that performs the numerical integration to update the
  // lander's pose. The time step is delta_t (global variable).
{
  // INSERT YOUR CODE HERE

	vector3d a = vector3d();

	//update historical positions
	second_last_position = last_position;
	last_position = position;

	//Calculate accelerations from forces

	a = (thrust_wrt_world() + Gravitational_Force(&position) + Drag_Force_Lander(&velocity, &position)) / getLanderMass();

	//position = position + delta_t * velocity;
	//velocity = velocity + delta_t * a;

	position = (2 * last_position) - (second_last_position)+((delta_t * delta_t) * a);
	velocity = (position - last_position) / (delta_t);

  // Here we can apply an autopilot to adjust the thrust, parachute and attitude
  if (autopilot_enabled) autopilot();

  // Here we can apply 3-axis stabilization to ensure the base is always pointing downwards
  if (stabilized_attitude) attitude_stabilization();
}

void initialize_simulation (void)
  // Lander pose initialization - selects one of 10 possible scenarios
{
  // The parameters to set are:
  // position - in Cartesian planetary coordinate system (m)
  // velocity - in Cartesian planetary coordinate system (m/s)
  // orientation - in lander coordinate system (xyz Euler angles, degrees)
  // delta_t - the simulation time step
  // boolean state variables - parachute_status, stabilized_attitude, autopilot_enabled
  // scenario_description - a descriptive string for the help screen

  scenario_description[0] = "circular orbit";
  scenario_description[1] = "descent from 10km";
  scenario_description[2] = "elliptical orbit, thrust changes orbital plane";
  scenario_description[3] = "polar launch at escape velocity (but drag prevents escape)";
  scenario_description[4] = "elliptical orbit that clips the atmosphere and decays";
  scenario_description[5] = "descent from 200km";
  scenario_description[6] = "aerostationary orbit";
  scenario_description[7] = "";
  scenario_description[8] = "";
  scenario_description[9] = "";

  switch (scenario) {

  case 0:
    // a circular equatorial orbit
    position = vector3d(1.2*MARS_RADIUS, 0.0, 0.0);
    velocity = vector3d(0.0, -3247.087385863725, 0.0);
    orientation = vector3d(0.0, 90.0, 0.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = false;
	stabilized_attitude_angle = 0;
    autopilot_enabled = false;
	safety_optimised = false;
	safety_enabled = false;
	orbital_reentry = true;
	orbital_reentry_count = 0;
	//indicateEulerReset(&second_last_position);
	generateInitialConditions(&second_last_position);
    break;

  case 1:
    // a descent from rest at 10km altitude
    position = vector3d(0.0, -(MARS_RADIUS + 10000.0), 0.0);
    velocity = vector3d(0.0, 0.0, 0.0);
    orientation = vector3d(0.0, 0.0, 90.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = true;
	stabilized_attitude_angle = 0;
    autopilot_enabled = false;
	generateInitialConditions(&second_last_position);
	safety_optimised = false;
	safety_enabled = false;
	orbital_reentry = false;
	orbital_reentry_count = 0;
    break;

  case 2:
    // an elliptical polar orbit
    position = vector3d(0.0, 0.0, 1.2*MARS_RADIUS);
    velocity = vector3d(3500.0, 0.0, 0.0);
    orientation = vector3d(0.0, 0.0, 90.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = false;
	stabilized_attitude_angle = 0;
    autopilot_enabled = false;
	generateInitialConditions(&second_last_position);
	safety_optimised = false;
	safety_enabled = false;
	orbital_reentry = true;
	orbital_reentry_count = 0;


	//orbital_reentry = 0;

    break;

  case 3:
    // polar surface launch at escape velocity (but drag prevents escape)
    position = vector3d(0.0, 0.0, MARS_RADIUS + LANDER_SIZE/2.0);
    velocity = vector3d(0.0, 0.0, 5027.0);
    orientation = vector3d(0.0, 0.0, 0.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = false;
	stabilized_attitude_angle = 0;
    autopilot_enabled = false;
	generateInitialConditions(&second_last_position);
	safety_optimised = false;
	safety_enabled = false;
	orbital_reentry = false;
	orbital_reentry_count = 0;



    break;

  case 4:
    // an elliptical orbit that clips the atmosphere each time round, losing energy
    position = vector3d(0.0, 0.0, MARS_RADIUS + 100000.0);
    velocity = vector3d(4000.0, 0.0, 0.0);
    orientation = vector3d(0.0, 90.0, 0.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = false;
	stabilized_attitude_angle = 0;
    autopilot_enabled = false;
	generateInitialConditions(&second_last_position);
	safety_enabled = false;
	safety_optimised = false;
	orbital_reentry = true;
	orbital_reentry_count = 0;

    break;

  case 5:
    // a descent from rest at the edge of the exosphere
    position = vector3d(0.0, -(MARS_RADIUS + EXOSPHERE), 0.0);
    velocity = vector3d(0.0, 0.0, 0.0);
    orientation = vector3d(0.0, 0.0, 90.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = true;
	stabilized_attitude_angle = 0;
    autopilot_enabled = false;
	generateInitialConditions(&second_last_position);
	safety_enabled = false;
	safety_optimised = false;

    break;

  case 6:
	  // aerostationary orbit
	  position = vector3d(0.0, (MARS_RADIUS + 17031e3), 0.0);
	  velocity = vector3d(1447.9, 0.0, 0.0);
	  orientation = vector3d(0.0, 90.0, 0.0);
	  delta_t = 0.1;
	  parachute_status = NOT_DEPLOYED;
	  stabilized_attitude = false;
	  stabilized_attitude_angle = 0;
	  autopilot_enabled = false;
	  generateInitialConditions(&second_last_position);
	  safety_enabled = false;
	  safety_optimised = false;
	  orbital_reentry = true;
	  orbital_reentry_count = 0;

    break;

	//Training case used to optimise for least time
  case 7:
	  position = vector3d(0.0, (MARS_RADIUS+LANDER_HEIGHT), 0.0);
	  velocity = vector3d(0, 0.0, 0.0);
	  orientation = vector3d(0.0, 0.0, 90.0);
	  delta_t = 0.1;
	  parachute_status = NOT_DEPLOYED;
	  stabilized_attitude = true;
	  stabilized_attitude_angle = 0;
	  autopilot_enabled = false;
	  throttle = 1;
	  generateInitialConditions(&second_last_position);
    break;

  case 8:
    break;

  case 9:
    break;

  }
}
