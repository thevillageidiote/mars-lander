#pragma once


#include <fstream>
#include "util_textures.h"
#include "lander.h"


Image loadPPM(const char *filename)
{

	FILE * file;

	Image output;

	file = fopen(filename, "rb");

	char info[54];

	fgets(info, sizeof(info), file);

	if (info[0] == 'P' && info[1] == '6')
	{

		//Collect next line
		fgets(info, sizeof(info), file);
		char * token = strtok(info, " ");
		output.width = atoi(token);
		token = strtok(info, " ");
		output.height = atoi(token);

		fgets(info, sizeof(info), file);

		//Read the rest of the file

		int size = 3 * output.width * output.height;
		output.data = new unsigned char[size]; // allocate 3 bytes per pixel
		fread(output.data, sizeof(unsigned char), size, file); // read the rest of the data at once
		fclose(file);
			
	}

	return output;

}

Image loadPGM(const char *filename)
{

	FILE * file;

	Image output;

	file = fopen(filename, "rb");

	char info[54];

	fgets(info, sizeof(info), file);

	if (info[0] == 'P' && info[1] == '5')
	{

		//Collect next line
		fgets(info, sizeof(info), file);
		char * token = strtok(info, " ");
		output.width = atoi(token);
		token = strtok(info, " ");
		output.height = atoi(token);

		//Read the rest of the file
		int size = output.width * output.height;
		output.data = new unsigned char[size]; // allocate 1 bytes per pixel
		fread(output.data, sizeof(unsigned char), size, file); // read the rest of the data at once
		fclose(file);

	}

	return output;

}





Image loadImage(const char *filename)
{

	FILE * file;

	GLuint texture;

	Image output;

	output.width = 0;
	output.height = 0;


	char buffer[4];

	int width, height;

	file = fopen(filename, "rb");

	unsigned char info[54];
	fread(info, sizeof(unsigned char), 54, file); // read the 54-byte header

												  // extract image height and width from header
	output.width = *(int*)&info[18];
	output.height = *(int*)&info[22];

	int size = 3 * output.width * output.height;
	output.data = new unsigned char[size]; // allocate 3 bytes per pixel
	fread(output.data, sizeof(unsigned char), size, file); // read the rest of the data at once
	fclose(file);


	for (int i = 0; i < output.width * output.height; ++i)
	{
		int index = i * 3;
		unsigned char B, R;
		B = output.data[index];
		R = output.data[index + 2];

		output.data[index] = R;
		output.data[index + 2] = B;

	}
	/*
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	
	//gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);


	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	*/
	return output;

}



GLuint loadTexture(const char *filename)
{

	GLuint texture;

	FILE * file;

	char buffer[4];

	int width, height;

	file = fopen(filename, "rb");

	unsigned char info[54];
	fread(info, sizeof(unsigned char), 54, file); // read the 54-byte header

												  // extract image height and width from header
	width = *(int*)&info[18];
	height = *(int*)&info[22];

	int size = 3 * width * height;
	unsigned char* data = new unsigned char[size]; // allocate 3 bytes per pixel
	fread(data, sizeof(unsigned char), size, file); // read the rest of the data at once
	fclose(file);


	for (int i = 0; i < width * height; ++i)
	{
		int index = i * 3;
		unsigned char B, R;
		B = data[index];
		R = data[index + 2];

		data[index] = R;
		data[index + 2] = B;

	}


	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);


	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE, data);
	free(data);

	return texture;
}

