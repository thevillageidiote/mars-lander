inFileName = "a.obj";
outFileName = "out.obj";


#open input file
file = open(inFileName,"r");

#erase output file
outfile = open(outFileName,"w");
outfile.close();

#open output file for append
outfile = open(outFileName,"a");


for line in file:
    if line[0] == 'o':
        temp = line.split();

        output = "g " + temp[1] + '\n';
    else:
        output = line;

    outfile.write(output)

file.close()
outfile.close()

