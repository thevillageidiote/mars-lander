


/*
double vertical_Kh = 0.0405;
double Kp = 1;
double delta = 0.5;
vertical_Kh = 0.0018; // For scenario 3
			 //vertical_Kh = 0.0020;

			 //Kp = 100;
			 //double desired_velocity = (-1e-17)*pow(altitude,4.0) + (5e-12)*pow(altitude, 3.0)  + (-7e-07)*pow(altitude, 2.0) + (0.0455)*pow(altitude, 1.0) + 15;
			 //double coeffs[] = { 3.03340261e-12, -6.05233537e-07,   4.43877236e-02,   1.67412551e+01 };
			 //double desired_velocity = coeffs[0] * pow(altitude, 3.0) + coeffs[1] * pow(altitude, 2.0) + coeffs[2] * pow(altitude, 1.0) + coeffs[3];
			 //cout << desired_velocity << "\n";

double h = position.abs() - MARS_RADIUS;

vector3d s = position.norm();

double vertical_velocity = velocity * (position.norm());
double desired_velocity = (0.5 + vertical_Kh * h);
double error = -(desired_velocity + vertical_velocity);
double Pout = Kp * error;

// Direction of tangential velocity - this must map to the world x-axis
// This assumes that the craft is not moving vertically. 
vector3d tv = velocity - (velocity*s)*s;

if (tv.abs() > 0.5)
{
	//Transverse velocity is not near zero, hence if satellite is still up it is likely to be in orbit. 
	//Begin orbital reentry code

	//turn off attitude stabilization
	stabilized_attitude = 0;

	//Point craft in opposite direction to motion
	setOrientation(-tv.norm());

	//Obtain horizontal_Pout based on a quick and rough P loop
	double horizontal_error = tv.abs();
	double horizontal_Pout = Kp * horizontal_error;

	//Set throttle based on horizontal_Pout
	if (horizontal_Pout <= 0) throttle = 0;
	if (horizontal_Pout >= 1) throttle = 1;
	else throttle = horizontal_Pout;
}

else if (tv.abs() <= 0.5)
{
	stabilized_attitude = 1;
	stabilized_attitude_angle = 0;

	if (Pout <= -delta)	throttle = 0;
	else if (Pout >= (1 - delta))
	{
		throttle = 1;
		if (safe_to_deploy_parachute() && h < 5000)	parachute_status = DEPLOYED;
	}
	else
	{
		throttle = Pout + delta;
		if (safe_to_deploy_parachute()) parachute_status = DEPLOYED;
	}
}

*/