#include "math.h"
#include "lander.h"
#include "util_functions.h"


void indicateEulerReset(vector3d *second_last_position) {
	(*second_last_position).x = -1;
	(*second_last_position).y = -1;
	(*second_last_position).z = -1;
}

void generateInitialConditions(vector3d *second_last_position)
{
	vector3d a = vector3d();

	a = (Gravitational_Force(&position) + Drag_Force_Lander(&velocity, &position)) / getLanderMass();
	position = position + delta_t * velocity;
	velocity = velocity + delta_t * a;

	//first position update
	last_position = position;

	a = (Gravitational_Force(&position) + Drag_Force_Lander(&velocity, &position)) / getLanderMass();
	position = position + delta_t * velocity;
	velocity = velocity + delta_t * a;

	*second_last_position = last_position;
	last_position = position;

}





double getLanderMass(void) {
	return (UNLOADED_LANDER_MASS + fuel*FUEL_CAPACITY*FUEL_DENSITY);
}


vector3d Gravitational_Force(vector3d *position) {

	return ((-GRAVITY * MARS_MASS * getLanderMass()) / (*position).abs2()) * (*position).norm();

}


vector3d Drag_Force_Lander(vector3d *velocity, vector3d *position) {

	return (-0.5 * DRAG_COEF_LANDER * atmospheric_density(*position) * M_PI * LANDER_SIZE * LANDER_SIZE * (*velocity).abs2()) * (*velocity).norm();

}

vector3d Drag_Force_Parachute(vector3d *velocity, vector3d *position) {

	return (-0.5 * DRAG_COEF_CHUTE * atmospheric_density(*position) * 5.0 * 2.0 * LANDER_SIZE * 2.0 * LANDER_SIZE * (*velocity).abs2()) * (*velocity).norm();

}


