#pragma once

#include <GL/glut.h>

struct Image {
	unsigned char * data;
	int width, height;
};


GLuint loadTexture(const char *filename);
Image loadImage(const char *filename);
Image loadPPM(const char *filename);
Image loadPGM(const char *filename);





